package com.example.shoppingcart.ui.cartlist

import android.os.Build.VERSION_CODES.M
import android.util.EventLog
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.shoppingcart.data.repository.ProductRepository
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.util.Event
import javax.inject.Inject

class CartListViewModel @Inject constructor(private val productRepository: ProductRepository) : ViewModel() {

    val cartList = MutableLiveData<List<Product>>()

    private val _deleteCartListEvent = MutableLiveData<Event<Product>>()

    val isVisible = MutableLiveData<Boolean>()

    val deleteCartEvent: LiveData<Event<Product>>
        get() = _deleteCartListEvent

    fun deleteCratItem(product: Product) {
        productRepository.deleteCratItem(product)
        _deleteCartListEvent.value = Event(product)
    }

    fun fetchCartList() {
        isVisible.value = productRepository.getCartList().isNullOrEmpty()
        cartList.value = productRepository.getCartList()
    }

    fun clearCartList() {
        productRepository.clearCartList()
        fetchCartList()
    }

    fun getTextFromAssets(): String {
        return productRepository.textFromAssets()
    }

    fun deleteCartItem(position: Int) {
        productRepository.deleteCartOnSwipe(position)
        fetchCartList()
    }

}