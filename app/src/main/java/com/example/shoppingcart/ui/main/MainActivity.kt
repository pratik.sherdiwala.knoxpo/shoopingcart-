package com.example.shoppingcart.ui.main

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppingcart.ProductNavigator
import com.example.shoppingcart.R
import com.example.shoppingcart.databinding.ActivityMainBinding
import com.example.shoppingcart.ui._common.DataBindingActivity
import com.example.shoppingcart.ui.cartlist.CartListFragment
import com.example.shoppingcart.ui.cartlist.adapter.CartAdapter
import com.example.shoppingcart.ui.productdetail.ProductDetailActivity
import com.example.shoppingcart.ui.productlist.ProductListFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

private val TAG = MainActivity::class.java.simpleName

class MainActivity : DataBindingActivity<ActivityMainBinding>(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector


    override val layoutId = R.layout.activity_main

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "Main onCreate()")
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        activity.openMainActivity()

        with(binding) {

            lifecycleOwner = activity
            viewModel = activity.viewModel

            setSupportActionBar(toolBar)

            navView.setNavigationItemSelectedListener { menuItem ->
                menuItem.isChecked = true
                drawerLayout.closeDrawers()
                handleDrawerLayoutItems(menuItem.itemId)
                true
            }
        }

        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }


        with(viewModel) {


        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {

            android.R.id.home -> {
                binding.drawerLayout.openDrawer(GravityCompat.START)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun handleDrawerLayoutItems(itemId: Int) {
        when (itemId) {
            R.id.home -> {
                openMainActivity()
            }
            R.id.cartList -> {
                openCartList()
            }
        }
    }


     fun openCartList() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, CartListFragment())
            .commit()
    }

    fun openMainActivity() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, ProductListFragment())
            .commit()
    }
}