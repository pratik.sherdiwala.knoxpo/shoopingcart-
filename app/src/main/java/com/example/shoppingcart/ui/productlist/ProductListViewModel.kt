package com.example.shoppingcart.ui.productlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.shoppingcart.data.repository.ProductRepository
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.util.Event
import javax.inject.Inject

class ProductListViewModel @Inject constructor(private val productRepository: ProductRepository) : ViewModel() {

    val productList = MutableLiveData<List<Product>>()

    //Open Product Detail Event
    private val _openProductDetail = MutableLiveData<Event<Int>>()

    val openProductDetail: LiveData<Event<Int>>
        get() = _openProductDetail

    fun openProductDetail(productId: Int) {
        _openProductDetail.value = Event(productId)
    }

    fun fetchProductList() {
        productList.value = productRepository.getProducts()
    }
}