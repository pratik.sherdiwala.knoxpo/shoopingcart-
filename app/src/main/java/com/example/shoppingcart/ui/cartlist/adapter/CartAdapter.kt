package com.example.shoppingcart.ui.cartlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingcart.R
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.ui.cartlist.CartListViewModel
import com.example.shoppingcart.ui.cartlist.adapter.viewholder.CartVH

class CartAdapter(private val cartListViewModel: CartListViewModel) : RecyclerView.Adapter<CartVH>(),
    ItemTouchHelperAdapter {

    private var cartList: List<Product>? = null

    private val ITEM_EMPTY_CART = 0
    private val ITEM_CART = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartVH {

        return CartVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_cart_list,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = cartList?.size ?: 0

    override fun onBindViewHolder(holder: CartVH, position: Int) {
        holder.bindCart(
            cartList!![position], {
                cartListViewModel.deleteCratItem(it)
            },
            cartListViewModel.getTextFromAssets()
        )
    }

    fun updateCartItems(newItem: List<Product>) {
        cartList = newItem
        notifyDataSetChanged()
    }

    override fun onItemDismiss(position: Int) {
        cartListViewModel.deleteCartItem(position)
        notifyItemRemoved(position)
    }
}
