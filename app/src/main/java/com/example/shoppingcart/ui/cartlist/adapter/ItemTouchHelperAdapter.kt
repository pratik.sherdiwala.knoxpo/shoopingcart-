package com.example.shoppingcart.ui.cartlist.adapter

import com.example.shoppingcart.model.Product

interface ItemTouchHelperAdapter {

    fun onItemDismiss(position:Int)

}