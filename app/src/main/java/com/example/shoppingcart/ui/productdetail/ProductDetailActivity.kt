package com.example.shoppingcart.ui.productdetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.bookappdynamic.viewmodel.AppViewModelFactory
import com.example.shoppingcart.R
import com.example.shoppingcart.databinding.ActivityMainBinding
import com.example.shoppingcart.databinding.ActivityProductDetailBinding
import com.example.shoppingcart.ui._common.DataBindingActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_product_detail.*
import javax.inject.Inject


class ProductDetailActivity : DataBindingActivity<ActivityProductDetailBinding>() {

    companion object {

        private val TAG = ProductDetailActivity::class.java.simpleName

        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"
        private val EXTRA_PRODUCT_ID = "$TAG.EXTRA_PRODUCT_ID"

        private const val ACTION_OPEN = 0

        fun intentForOpenProductDetail(context: Context, productId: Int): Intent {
            return Intent(context, ProductDetailActivity::class.java)
                .apply {
                    putExtra(EXTRA_PRODUCT_ID, productId)
                    putExtra(EXTRA_ACTION, ACTION_OPEN)
                }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(ProductDetailViewModel::class.java)
    }

    override val layoutId = R.layout.activity_product_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate()")
        val activity = this

        with(binding) {
            viewModel = activity.viewModel
        }

        val id = intent.getIntExtra(EXTRA_PRODUCT_ID, -1)
        viewModel.getProduct(id)

        viewModel.viewPagerLoaderEvent.observe(this,
            Observer {
                it.getContentIfNotHandled()?.let {
                    setUpViewPager(binding.viewPager)
                }
            })

        addCartBTN.setOnClickListener {
            if(viewModel.addToCart()){
                Toast.makeText(activity,"Product Added",Toast.LENGTH_SHORT).show()

                finish()
            } else {
                Toast.makeText(activity,"Product Already Added",Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    private fun setUpViewPager(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(supportFragmentManager)

        Log.d(TAG, "View Pager")

        viewModel.product.value?.images?.forEach {
            adapter.addFragment(ProductImageFragment.newInstance(it))
        }

        viewPager.adapter = adapter
    }
}