package com.example.shoppingcart.ui.productlist.adapter.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingcart.R
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.util.GlideApp

class ProductVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mPriceTV = itemView.findViewById<TextView>(R.id.priceTV)
    private val mImageIV = itemView.findViewById<ImageView>(R.id.image)

    fun bindProduct(
        product: Product,
        productClicked: (product: Product) -> Unit
    ) {

        mNameTV.text = product.name
        mPriceTV.text = "$" + product.price

        if (!product.images.isNullOrEmpty()) {
            GlideApp.with(itemView)
                .load(product.images?.get(0))
                .error(R.mipmap.ic_launcher_round)
                .into(mImageIV)
        } else {
            GlideApp.with(itemView)
                .load(R.mipmap.ic_launcher_round)
                .into(mImageIV)
        }


        itemView.setOnClickListener {
            productClicked(product)
        }
    }

}