package com.example.shoppingcart.ui.productlist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bookappdynamic.viewmodel.AppViewModelFactory
import com.example.shoppingcart.ProductNavigator
import com.example.shoppingcart.R
import com.example.shoppingcart.databinding.ActivityMainBinding
import com.example.shoppingcart.databinding.FragmentProductListBinding
import com.example.shoppingcart.ui.main.MainActivity
import com.example.shoppingcart.ui.productdetail.ProductDetailActivity
import com.example.shoppingcart.ui.productlist.adapter.ProductListAdapter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_product_list.*
import javax.inject.Inject

class ProductListFragment : Fragment(), ProductNavigator {

    private val REQUEST_CODE_OPEN_PRODCUTDETAIL = 0

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentProductListBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(ProductListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel

            productsRV.layoutManager = GridLayoutManager(context, 2)
            productsRV.adapter = ProductListAdapter(fragment.viewModel)
        }

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = false
        }

        with(viewModel) {
            fetchProductList()

            openProductDetail.observe(fragment,
                Observer {
                    it.getContentIfNotHandled()?.let {
                        fragment.openProductDetail(it)
                    }
                })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.binding = FragmentProductListBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun openProductDetail(id: Int) {
        startActivityForResult(
            ProductDetailActivity.intentForOpenProductDetail(
                activity!!,
                id
            ),
            REQUEST_CODE_OPEN_PRODCUTDETAIL
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_OPEN_PRODCUTDETAIL -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.fetchProductList()
                }
            }
        }
    }
}