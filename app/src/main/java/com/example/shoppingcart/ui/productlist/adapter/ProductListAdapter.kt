package com.example.shoppingcart.ui.productlist.adapter

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingcart.R
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.ui.productlist.ProductListViewModel
import com.example.shoppingcart.ui.productlist.adapter.viewholder.ProductVH

class ProductListAdapter(private val productListViewModel: ProductListViewModel) : RecyclerView.Adapter<ProductVH>() {

    private var productList: List<Product>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVH {
        return ProductVH(
            LayoutInflater.from(
                parent.context
            ).inflate(
                R.layout.item_product_list,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = productList?.size ?: 0

    override fun onBindViewHolder(holder: ProductVH, position: Int) {
        holder.bindProduct(productList!![position]) {
            Log.d(ContentValues.TAG, "Item Clicked ${it.id}")
            productListViewModel.openProductDetail(it.id)
        }
    }

    fun updateProduct(newProduct: List<Product>) {
        productList = newProduct
        notifyDataSetChanged()
    }
}
