package com.example.shoppingcart.ui.productdetail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.shoppingcart.R
import com.example.shoppingcart.util.GlideApp

class ProductImageFragment : Fragment() {

    companion object {

        private val TAG = ProductImageFragment::class.java.simpleName
        private val ARGS_IMAGE_URL = "$TAG.ARGS_IMAGE_URL"

        fun newInstance(imagesUrl: String): ProductImageFragment {
            val fragment = ProductImageFragment()
            val args = Bundle()
            args.putString(ARGS_IMAGE_URL, imagesUrl)
            fragment.arguments = args
            return fragment
        }
    }

    private val imageUrl: String
        get() {
            return arguments!!.getString(ARGS_IMAGE_URL)!!
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d(TAG, "Loading Imageurl $imageUrl")

        val mImageIV = view.findViewById<ImageView>(R.id.imageIV)

        GlideApp.with(this)
            .load(imageUrl)
            .error(R.mipmap.ic_launcher)
            .into(mImageIV)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "Loading Imageurl $imageUrl")

        return inflater.inflate(R.layout.fragment_product_image, container, false)
    }
}