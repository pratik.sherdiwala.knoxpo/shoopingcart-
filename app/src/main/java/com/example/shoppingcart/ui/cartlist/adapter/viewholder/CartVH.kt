package com.example.shoppingcart.ui.cartlist.adapter.viewholder

import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingcart.R
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.util.GlideApp
import kotlinx.android.synthetic.main.item_cart_list.view.*

class CartVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.cartnameTV)
    private val mImageIV = itemView.findViewById<AppCompatImageView>(R.id.cartImage)
    private val mRemoveBTN = itemView.findViewById<ImageButton>(R.id.removeCartBTN)

    fun bindCart(
        product: Product,
        deleteProduct: (product: Product) -> Unit,
        value: String
    ) {
        mNameTV.text = product.name

        if (!product.images.isNullOrEmpty()) {
            GlideApp.with(itemView)
                .load(product.images?.get(0))
                .error(R.mipmap.ic_launcher_round)
                .into(mImageIV)
        } else {
            GlideApp.with(itemView)
                .load(R.mipmap.ic_launcher_round)
                .into(mImageIV)
        }

        mRemoveBTN.setOnClickListener {
            deleteProduct(product)
        }

        Log.d("CartVH","text $value")
    }
}