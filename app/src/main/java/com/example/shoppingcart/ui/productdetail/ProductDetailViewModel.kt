package com.example.shoppingcart.ui.productdetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.shoppingcart.util.Event
import com.example.shoppingcart.data.repository.ProductRepository
import com.example.shoppingcart.model.Product
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductDetailViewModel @Inject constructor(private val productRepository: ProductRepository) : ViewModel() {

    private val TAG = ProductDetailViewModel::class.java.simpleName

    private val _viewPagerLoaderEvent = MutableLiveData<Event<Unit>>()

    val viewPagerLoaderEvent: LiveData<Event<Unit>>
        get() = _viewPagerLoaderEvent

    val product = MutableLiveData<Product>()

    private val disposable = CompositeDisposable()

    fun getProduct(id: Int) {

        productRepository.getProductDetail(id)
            .subscribe(
                {
                    product.value = it
                    _viewPagerLoaderEvent.value = Event(Unit)
                    Log.d(TAG, "getProduct($id")
                },
                {
                    Log.d(TAG, "Error in Fetching Products", it)
                }
            ).also {
                disposable.add(it)
            }
    }

    fun addToCart(): Boolean {
        return productRepository.addToCartList(product.value!!)
    }

    override fun onCleared() {
        disposable.dispose()
    }
}