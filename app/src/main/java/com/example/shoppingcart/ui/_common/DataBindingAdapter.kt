package com.example.shoppingcart.ui._common

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingcart.model.Product
import com.example.shoppingcart.ui.cartlist.adapter.CartAdapter
import com.example.shoppingcart.ui.productlist.adapter.ProductListAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("products")
    fun setProducts(view: RecyclerView, products: List<Product>?) {
        products?.let {
            (view.adapter as? ProductListAdapter)?.updateProduct(it)
        }
    }

    @JvmStatic
    @BindingAdapter("price")
    fun setPrice(view: TextView, price: Double?) {
        price?.let {
            view.text = price.toString()
        }
    }

    @JvmStatic
    @BindingAdapter("cart")
    fun setCartItems(view:RecyclerView, cartList:List<Product>?){
        cartList?.let {
            (view.adapter as CartAdapter)?.updateCartItems(it)
        }
    }
}