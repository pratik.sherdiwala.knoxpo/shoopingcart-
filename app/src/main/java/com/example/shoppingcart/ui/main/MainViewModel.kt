package com.example.shoppingcart.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.shoppingcart.util.Event
import com.example.shoppingcart.data.repository.ProductRepository
import com.example.shoppingcart.model.Product
import javax.inject.Inject

private val TAG = MainViewModel::class.java.simpleName

class MainViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    private val _openProductDetail = MutableLiveData<Event<Int>>()

    val openProductDetail: LiveData<Event<Int>>
        get() = _openProductDetail

    private val _openDrawerEvent = MutableLiveData<Event<Unit>>()

    val openDrawerEvent: LiveData<Event<Unit>>
        get() = _openDrawerEvent

    fun oepnDrawer() {
        _openDrawerEvent.value = Event(Unit)
    }


    fun openProductDetail(productId: Int) {
        _openProductDetail.value = Event(productId)
        Log.d(TAG, "MainVM openProductDetail()")
    }
}