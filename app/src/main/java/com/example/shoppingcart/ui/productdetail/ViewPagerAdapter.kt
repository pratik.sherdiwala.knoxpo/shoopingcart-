package com.example.shoppingcart.ui.productdetail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val fragment = ArrayList<Fragment>()

    fun addFragment(fm: Fragment) {
        fragment.add(fm)
     }

    override fun getItem(position: Int) = fragment[position]

    override fun getCount() = fragment.size
}