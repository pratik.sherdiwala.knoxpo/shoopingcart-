package com.example.shoppingcart.ui.cartlist

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppingcart.R
import com.example.shoppingcart.databinding.FragmentCartBinding
import com.example.shoppingcart.ui.cartlist.adapter.CartAdapter
import com.example.shoppingcart.ui.cartlist.adapter.SimpleItemTouchHelperCallback
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_cart.*
import javax.inject.Inject

class CartListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentCartBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(CartListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this

        with(binding) {
            viewModel = fragment.viewModel
            val adapter = CartAdapter(fragment.viewModel)
            cartsRV.layoutManager = LinearLayoutManager(context)
            cartsRV.adapter = adapter
            val callBack = SimpleItemTouchHelperCallback(adapter)
            val itemTouchHelper = ItemTouchHelper(callBack)
            itemTouchHelper.attachToRecyclerView(cartsRV)
        }

        with(viewModel) {
            fetchCartList()

            deleteCartEvent.observe(fragment,
                Observer {
                    it.getContentIfNotHandled()?.let {
                        fetchCartList()
                    }
                })
        }

        //emptyBagTV.text = viewModel.getTextFromAssets()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.binding = FragmentCartBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.item_clear_cartlist, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.clearBTN -> {
                viewModel.clearCartList()
            }
        }

        return super.onOptionsItemSelected(item)
    }

}