package com.example.shoppingcart.di.module

import com.example.shoppingcart.ui.productdetail.ProductDetailActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProductDetailModule {

   @ContributesAndroidInjector
   abstract fun contributeProductDetailActivity():ProductDetailActivity


}