package com.example.shoppingcart.di

import android.app.Application
import com.example.shoppingcart.App
import com.example.shoppingcart.di.module.ContextModule
import com.example.shoppingcart.di.module.MainActivityModule
import com.example.shoppingcart.di.module.ProductDetailModule
import com.example.shoppingcart.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        ProductDetailModule::class,
        ContextModule::class
    ]
)

interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun context(context:Application):Builder

        fun create():AppComponent
    }


    fun inject(app: App)

}