package com.example.shoppingcart.di.module

import com.example.shoppingcart.ui.cartlist.CartListFragment
import com.example.shoppingcart.ui.main.MainActivity
import com.example.shoppingcart.ui.productlist.ProductListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(
        modules = [
            ProductListModule::class,
            CartListModule::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity

}

@Module
abstract class ProductListModule {

    @ContributesAndroidInjector
    abstract fun contributeProductListFragment(): ProductListFragment

}

@Module
abstract class CartListModule {

    @ContributesAndroidInjector
    abstract fun contributeCartListFragment(): CartListFragment
}

