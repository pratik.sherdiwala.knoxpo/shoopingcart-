package com.example.shoppingcart.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bookappdynamic.viewmodel.AppViewModelFactory
import com.example.shoppingcart.ui.cartlist.CartListViewModel
import com.example.shoppingcart.ui.main.MainViewModel
import com.example.shoppingcart.ui.productdetail.ProductDetailViewModel
import com.example.shoppingcart.ui.productlist.ProductListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    abstract fun provideViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(prodctDetailViewModel: ProductDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CartListViewModel::class)
    abstract fun bindCartListViewModel(cartListViewModel: CartListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductListViewModel::class)
    abstract fun bindProductlistModel(productListViewModel: ProductListViewModel): ViewModel



}