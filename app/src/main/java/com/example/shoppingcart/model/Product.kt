package com.example.shoppingcart.model

data class Product(
    var id: Int,
    var name: String,
    var price: Double,
    var images: List<String>?
)