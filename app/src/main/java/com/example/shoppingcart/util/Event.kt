package com.example.shoppingcart.util

open class Event<out T>(private val content: T) {

    var hasBeenhandled = false
        private set

    fun getContentIfNotHandled(): T? {

        return if (hasBeenhandled) {
            null
        } else {
            hasBeenhandled = true
            content
        }
    }

    fun peekContent(): T = content

}