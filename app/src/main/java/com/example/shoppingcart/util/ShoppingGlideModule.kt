package com.example.shoppingcart.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class ShoppingGlideModule : AppGlideModule()