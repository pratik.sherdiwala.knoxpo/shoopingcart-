package com.example.shoppingcart

interface ProductNavigator {

    fun openProductDetail(id:Int)

}