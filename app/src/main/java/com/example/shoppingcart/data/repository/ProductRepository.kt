package com.example.shoppingcart.data.repository

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import com.example.shoppingcart.data.local.ProductLab
import com.example.shoppingcart.model.Product
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Single
import java.io.*
import java.lang.Exception
import java.nio.charset.Charset
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import com.google.gson.reflect.TypeToken


private val TAG = ProductRepository::class.java.simpleName

@Singleton
class ProductRepository @Inject constructor(
    private val productLab: ProductLab,
    private val context: Context
) {

    private val fileName = "products.json"

    fun getProducts(): List<Product> {
//        val productDetailsFromJson = readFileAsTextUsingInputString()
//        //Log.d(TAG, "Product Details $productDetailsFromJson")
//
//        //val productListType = object : TypeToken<List<Product>>() {}.type
//        val response = Gson().fromJson(productDetailsFromJson, ProductResponse::class.java)
//        Log.d(TAG, "Product Details $response")
//        return response.products
        return productLab.productList
    }

    fun getProductDetail(id: Int): Single<Product> {
        val product = productLab.productList.find {
            it.id == id
        }

        return if (product == null) {
            Single.error(Exception("Product not available"))
        } else {
            Single.just(product)
        }
    }


    fun addToCartList(product: Product): Boolean {

        if (!productLab.cartList.contains(product)) {
            productLab.cartList.add(product)
            return true
        }
        return false
    }

    fun getCartList(): List<Product> {
        return productLab.cartList
    }

    fun deleteCratItem(product: Product) {
        productLab.cartList.remove(product)
    }

    fun clearCartList() {
        productLab.cartList.clear()
    }

    fun textFromAssets(): String {
        val inputStreamReader = InputStreamReader(
            context.assets.open(
                "textFile"
            )
        )
        val bf = BufferedReader(inputStreamReader)
        val text = bf.readLine()
        return text
    }

    fun readFileAsTextUsingInputString() =
        context.assets.open(
            fileName
        ).readBytes().toString(Charset.defaultCharset())

    data class ProductResponse(val products: List<Product>)

    fun deleteCartOnSwipe(position: Int) {
        productLab.cartList.removeAt(position)
    }
}